import os
import myutils


def getSize(path):
    if os.path.isfile(path):
        return sum([int(len(line) < 3) for line in open(path)])
    return 0

def readConll(path):
    if not os.path.isfile(path):
        return []
    data = [[]]
    for line in open(path):
        if len(line) < 3:
            data.append([])
        else:
            data[-1].append(line)
    return data[:-1] # remove last empty one

def resize(data, beg, end, newPath):
    newFile = open(newPath, 'w')
    for conlSent in data[beg:end]:
        newFile.write(''.join(conlSent) + '\n')
    newFile.close()

tgtDir = 'data/newsplits-v2.8/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

for treebankDir in os.listdir('data/ud-treebanks-v2.8.noEUD'):
    tgtTreebankDir = tgtDir + treebankDir + '/'
    if not os.path.isdir(tgtTreebankDir):
        os.mkdir(tgtTreebankDir)
    trainPath, _, devPath, testPath = myutils.getTrainTuneDevTest('data/ud-treebanks-v2.8.noEUD/' + treebankDir)
    trainData = readConll(trainPath)
    devData = readConll(devPath)
    fullData = trainData + devData
    numSents = len(fullData)
    os.system('cp ' + testPath + ' ' + tgtTreebankDir + 'test.conllu')

    if numSents == 0:
        continue
    elif numSents < 3000:
        splitSize = int(numSents/4)
        resize(fullData, 0, int(splitSize*2), tgtTreebankDir + 'train.conllu')
        resize(fullData, int(splitSize*2), int(splitSize * 3), tgtTreebankDir + 'tune.conllu')
        resize(fullData, 0, int(splitSize * 3), tgtTreebankDir + 'trainPlusTune.conllu')
        resize(fullData, int(splitSize * 3), int(splitSize * 4), tgtTreebankDir + 'dev.conllu') 
    else:
        resize(fullData, 0, min(numSents-1500, 20000), tgtTreebankDir + 'train.conllu')
        resize(fullData, numSents-1500, numSents-750, tgtTreebankDir + 'tune.conllu')
        resize(fullData, 0, min(numSents-750, 20000), tgtTreebankDir + 'trainPlusTune.conllu')
        resize(fullData, numSents-750, numSents, tgtTreebankDir + 'dev.conllu')


