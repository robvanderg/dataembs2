import myutils
import ast
import os

def getScores(treebank, cluster):
    scores = []

    singlePath = 'preds/' + treebank + '.' + treebank 
    scores.append(myutils.path2score(singlePath))

    concatPath = 'preds/' + cluster + '.concat.' + treebank
    scores.append(myutils.path2score(concatPath))

    embedsOldPath = 'preds/' + cluster + '.decoder.' + treebank
    scores.append(myutils.path2score(embedsOldPath))

    embedsNewPath = 'preds/' + cluster + '.encoder.' + treebank
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/' + cluster + '.both.' + treebank
    scores.append(myutils.path2score(embedsNewPath))
    
    embedsNewPath = 'preds/all.concat.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.decoder.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.encoder.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.both.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    #scores.append(max(scores[2:])-max(scores[:2]))
    return scores

def toString(name, scores):
    string = name.replace('_', '\\_')
    
    for score in scores:
        string += ' & '
        if score == max(scores):
            string += '\\textbf{' + '{:.2f}'.format(score) + '}'
        else:
            string += '{:.2f}'.format(score)
    return string + ' \\\\'

def shortenName(treebank):
    for path in os.listdir('data/ud-treebanks-v2.8/' + treebank):
        if path.endswith('conllu'):
            return path.split('-')[0]
    return treebank

print('cluster & treebank & self & concat & decoder & encoder & both & ALLconc. & ALLdec. & ALLenc. & ALLboth & diff\\\\')
clusters = myutils.getClusters()
totals = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
counter = 0
for cluster in sorted(clusters):
    for treebankIdx, treebank in enumerate(sorted(clusters[cluster])):
        scores = getScores(treebank, cluster)
        if 0.0 not in scores:
            print('\\midrule \n' +cluster if treebankIdx == 0 else '', end=' & ')
            print(toString(shortenName(treebank), scores))
            counter += 1
            for i in range(len(scores)):
                totals[i] += scores[i]

print('\\midrule')
print(' & ' +toString('avg.', [x/counter for x in totals]))
        

