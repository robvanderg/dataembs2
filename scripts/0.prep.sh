
# get old and new implementation of dataset embeddings
git clone https://robvanderg@bitbucket.org/ahmetustunn/mtp.git
cd mtp
git reset --hard 8dcf760dde47a8e4f32cb72e170da16c48b523aa
cd ..
mv mtp mtp-new
git clone https://robvanderg@bitbucket.org/ahmetustunn/mtp.git
mv mtp mtp-old
sed -i "s;dataset_embeds_dim\": 0,;dataset_embeds_dim\": 32,;g" mtp-old/configs/params.json

# get data
wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3687/ud-treebanks-v2.8.tgz
tar -zxvf ud-treebanks-v2.8.tgz
mkdir data
mv ud-treebanks-v2.8 data
cp -r data/ud-treebanks-v2.8 data/ud-treebanks-v2.8.noEUD
python3 mtp-old/scripts/misc/cleanconl.py data/ud-treebanks-v2.8.noEUD/*/*conllu
python3 scripts/0.resplit.py

