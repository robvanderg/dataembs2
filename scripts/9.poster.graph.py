import myutils
import ast
import os

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


def getScores(treebank, cluster):
    scores = []

    singlePath = 'preds/' + treebank + '.' + treebank
    scores.append(myutils.path2score(singlePath))

    concatPath = 'preds/' + cluster + '.concat.' + treebank 
    scores.append(myutils.path2score(concatPath))

    embedsOldPath = 'preds/' + cluster + '.decoder.' + treebank 
    scores.append(myutils.path2score(embedsOldPath))

    embedsNewPath = 'preds/' + cluster + '.encoder.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/' + cluster + '.both.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))
    
    embedsNewPath = 'preds/all.concat.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.decoder.' + treebank
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.encoder.' + treebank
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.both.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    return scores

clusters = myutils.getClusters()

def getTrainSize(name):
    counter = 0
    for line in open('data/newsplits-v2.8/' + name + '/train.conllu'):
        if len(line) < 2:
            counter += 1
    return counter

def trainSize(data):
    small = ['small']
    medium = ['medium']
    large = ['large']
    for item in data:
        size = getTrainSize(item[0])
        if size < 1000:
            small.append(item)
        elif size < 10000:
            medium.append(item)
        else:
            large.append(item)
    return [small, medium, large]

def sameLang(data): 
    has = ['hasSameLang']
    hasNot = ['noSameLang']
    for item in data:
        lang = item[0].split('_')[1].split('-')[0]
        counter = 0
        for treebank in clusters[item[1]]:
            treebankLang = treebank.split('_')[1].split('-')[0]
            if treebankLang == lang:
                counter+=1
        if counter > 1:
            has.append(item)
        else:
            hasNot.append(item)
    return [has, hasNot]

def selfScore(data):
    low = ['low']
    medium = ['medium']
    high = ['high']
    for item in data:
        if item[2][0] < 50:
            low.append(item)
        elif item[2][0] < 80:
            medium.append(item)
        else:
            high.append(item)
    return low, medium, high

def clusterSize(data):
    small = ['cluster=2']
    large = ['cluster>2']
    for item in data:
        if len(clusters[item[1]]) > 2:
            large.append(item)
        else:
            small.append(item)
    return [small, large]

def avg(data):
    return [['avg.'] + data]
    

def average(data):
    totals = [0.0] * 9
    for treebank in data[1:]:
        for i in range(len(treebank[2])):
            totals[i] += treebank[2][i]
    for i in range(len(totals)):
        totals[i] = totals[i]/len(data[1:])
    return totals

for All in [True]:
    data = []
    for cluster in sorted(clusters):
        for treebankIdx, treebank in enumerate(sorted(clusters[cluster])):
            train, _, _, _ = myutils.getTrainTuneDevTest('data/newsplits-v2.8/' + treebank)
            if train == '':
                continue
    
            scores = getScores(treebank, cluster)
            data.append((treebank, cluster, scores))
    
    plt.style.use('scripts/niceGraphs.mplstyle')
    fig, ax = plt.subplots(figsize=(15,4), dpi=300)
    
    colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    colors = colors + colors
    
    names = ['mono', 'concat', 'decoder', 'encoder', 'both', 'concat', 'decoder', 'encoder', 'both']
    labeled = False
    idx = 0

    # first add average not All
    for group in avg(data):
        scores = average(group)
        targets = range(5)
        for forIdx, scoreIdx in enumerate(targets):#len(scores)):
            if labeled == False:
                ax.bar([idx+.15*forIdx], [scores[scoreIdx]], label=names[scoreIdx], color=colors[forIdx], width=.15)
            else:
                ax.bar([idx+.15*forIdx], [scores[scoreIdx]], color=colors[forIdx], width=.15)
        if labeled == False:
            labeled = True
        idx += 1

    #for splitFunction in [trainSize, sameLang, clusterSize, selfScore]:
    for splitFunction in [avg, sameLang, clusterSize, selfScore, trainSize]:
        for group in splitFunction(data):
            scores = average(group)
            print(scores)
            targets = range(5)
            if All:
                targets=[0,5,6,7,8]
            for forIdx, scoreIdx in enumerate(targets):#len(scores)):
                if labeled == False:
                    ax.bar([idx+.15*forIdx], [scores[scoreIdx]], label=names[scoreIdx], color=colors[forIdx], width=.15)
                else:
                    ax.bar([idx+.15*forIdx], [scores[scoreIdx]], color=colors[forIdx], width=.15)
            if labeled == False:
                labeled = True
            idx += 1
    leg = ax.legend(loc='lower right')
    leg.get_frame().set_linewidth(1.5)
    
    ax.plot([.8,.8], [0,100], linestyle='dashed', color='black')
    ax.plot([1.8,1.8], [0,100], linestyle='dashed', color='black')
    ax.plot([3.8,3.8], [0,100], linestyle='dashed', color='black')
    ax.plot([5.8,5.8], [0,100], linestyle='dashed', color='black')
    ax.plot([8.8,8.8], [0,100], linestyle='dashed', color='black')
    
    ax.set_xticks([0.3,1.3,2.3,3.3,4.3,5.3,6.3,7.3, 8.3, 9.3, 10.3, 11.3])
    ax.set_xticklabels(ax.get_xticks(), rotation = 25, ha='right')
    ax.set_xticklabels(['clusters', 'all', '+sameLang', '-sameLang', 'cluster==2', 'cluster>2', 'LAS<50', '50<LAS<80', 'LAS>80', 'small', 'medium', 'large'])
    
    
    ax.set_ylim(25,92)
    ax.set_xlim(-0.15,10.75)
    if All:
        fig.savefig('filtersAll.pdf', bbox_inches='tight')
    else:
        fig.savefig('filters.pdf', bbox_inches='tight')




#print('\\midrule')
#print(' & ' +toString('avg.', [x/counter for x in totals]))
        

