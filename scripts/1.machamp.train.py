import myutils
from allennlp.common import Params

def createConfigs(treebank):
    train, tune, dev, test = myutils.getTrainTuneDevTest('data/newsplits-v2.8/' + treebank)
    config = {}
    if train == '':
        return ''
    config['train_data_path'] = '../' + train
    if dev != '':
        config['validation_data_path'] = '../' + dev
    config['word_idx'] = 1
    config['tasks'] = {}
    if myutils.hasColumn(test, 3, threshold=.1):
        config['tasks']['upos'] = {'task_type':'seq', 'column_idx':3}
    if myutils.hasColumn(test, 2, threshold=.95):
        config['tasks']['lemma'] = {'task_type':'string2string', 'column_idx':2}
    if myutils.hasColumn(test, 5, threshold=.95):
        config['tasks']['feats'] = {'task_type':'seq', 'column_idx':5}
    config['tasks']['dependency'] = {'task_type':'dependency', 'column_idx':6}

    allennlpConfig = Params({treebank: config})
    jsonPath = 'configs/' + treebank + '.json'
    allennlpConfig.to_file(jsonPath)
    
    allennlpConfig[treebank]['dec_dataset_embed_idx'] = -1
    allennlpConfig.to_file(jsonPath.replace('.json', '-dec-embeds.json'))
    allennlpConfig[treebank]['enc_dataset_embed_idx'] = -1
    allennlpConfig.to_file(jsonPath.replace('.json', '-both-embeds.json'))

    del allennlpConfig[treebank]['dec_dataset_embed_idx']
    allennlpConfig.to_file(jsonPath.replace('.json', '-enc-embeds.json'))
    return jsonPath

def train(treebanks, name, decDataEmbs=False, encDataEmbs=False):
    if encDataEmbs and not decDataEmbs:
        treebanks = treebanks.replace('.json', '-enc-embeds.json')
    elif decDataEmbs and not encDataEmbs:
        treebanks = treebanks.replace('.json', '-dec-embeds.json')
    elif encDataEmbs and decDataEmbs:
        treebanks = treebanks.replace('.json', '-both-embeds.json')
    baseDir = 'mtp/'

    for seed in myutils.seeds:
        if myutils.getModel(baseDir + 'logs/' + name + '.' + seed) == '':
            cmd = 'cd ' + baseDir + ' && '
            cmd += 'python3 train.py --dataset_configs ' + treebanks + ' --name ' + name + '.' + seed + ' --seed ' + seed
            if decDataEmbs:
                cmd += ' --parameters_config configs/params.dataEmbs.json'
            print(cmd + ' && cd ../')

listOfAllTreebanks = ''
clusters = myutils.getClusters()
for cluster in clusters:
    listOfTreebanks = ''
    # single baseline:
    for treebank in clusters[cluster]:
        dataconfig_path = createConfigs(treebank)
        if dataconfig_path != '':
            train('../' + dataconfig_path, treebank, decDataEmbs = False, encDataEmbs = False)
            listOfTreebanks += '../' + dataconfig_path + ' '

    listOfAllTreebanks += listOfTreebanks

    # concat baseline:
    train(listOfTreebanks, cluster + '.concat', decDataEmbs = False, encDataEmbs = False)

    # dataset embeds decoder
    train(listOfTreebanks, cluster + '.decoder', decDataEmbs = True, encDataEmbs = False)

    # dataset embeds encoder
    train(listOfTreebanks, cluster + '.encoder', decDataEmbs = False, encDataEmbs = True)

    # dataset embeds both
    train(listOfTreebanks, cluster + '.both', decDataEmbs = True, encDataEmbs = True)

# on all treebanks:
# concat baseline:
train(listOfAllTreebanks, 'all.concat', decDataEmbs = False, encDataEmbs = False)
# dataset embeds new
train(listOfAllTreebanks, 'all.decoder', decDataEmbs = True, encDataEmbs = False)
# dataset embeds old
train(listOfAllTreebanks, 'all.encoder', decDataEmbs = False, encDataEmbs = True)
train(listOfAllTreebanks, 'all.both', decDataEmbs = True, encDataEmbs = True)


