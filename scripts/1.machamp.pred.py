import myutils

def pred(modelName, treebankName):
    for seed in myutils.seeds:
        model = myutils.getModel('mtp/logs/' + modelName + '.' + seed + '/')
        cmd = 'cd mtp && '

        train, _, dev, _ = myutils.getTrainTuneDevTest('data/newsplits-v2.8/' + treebankName + '/')
        if train == '':
            continue
        if model != '':
            model = model[model.find('/')+1:]
            outPath = '../preds/' + modelName + '.' + treebankName + '.' + seed
            cmd += 'python3 predict.py ' + model + ' ../' + dev +  ' ' + outPath + ' --dataset ' + treebankName
            cmd += ' > ' + outPath + '.eval && cd ..'
            print(cmd) 
        else:
            print("MODEL NOT FOUND: " + 'mtp-new/logs/' + modelName + '.' + seed + '/')

clusters = myutils.getClusters()
for cluster in clusters:
    for treebank in clusters[cluster]:
        
        # single
        pred(treebank, treebank)

        # concat baseline:
        pred(cluster + '.concat', treebank)

        # dataset embeds new
        pred(cluster + '.encoder', treebank)

        # dataset embeds old
        pred(cluster + '.decoder', treebank)

        # dataset embeds both
        pred(cluster + '.both', treebank)

        # concat baseline:
        pred('all.concat', treebank)
        # dataset embeds new
        pred('all.encoder', treebank)
        # dataset embeds old
        pred('all.decoder', treebank)
        pred('all.both', treebank)


