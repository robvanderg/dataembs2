import myutils
import ast
import os

def getScores(treebank, cluster):
    scores = []

    singlePath = 'preds/' + treebank + '.' + treebank 
    scores.append(myutils.path2score(singlePath))

    concatPath = 'preds/' + cluster + '.concat.' + treebank 
    scores.append(myutils.path2score(concatPath))

    embedsOldPath = 'preds/' + cluster + '.decoder.' + treebank 
    scores.append(myutils.path2score(embedsOldPath))

    embedsNewPath = 'preds/' + cluster + '.encoder.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/' + cluster + '.both.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))
    
    embedsNewPath = 'preds/all.concat.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.decoder.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.encoder.' + treebank
    scores.append(myutils.path2score(embedsNewPath))

    embedsNewPath = 'preds/all.both.' + treebank 
    scores.append(myutils.path2score(embedsNewPath))

    return scores

clusters = myutils.getClusters()

def getTrainSize(name):
    counter = 0
    for line in open('data/newsplits-v2.8/' + name + '/train.conllu'):
        if len(line) < 2:
            counter += 1
    return counter

def trainSize(data):
    small = ['small']
    medium = ['medium']
    large = ['large']
    for item in data:
        size = getTrainSize(item[0])
        if size < 1000:
            small.append(item)
        elif size < 10000:
            medium.append(item)
        else:
            large.append(item)
    return [small, medium, large]

def sameLang(data): 
    has = ['hasSameLang']
    hasNot = ['noSameLang']
    for item in data:
        lang = item[0].split('_')[1].split('-')[0]
        counter = 0
        for treebank in clusters[item[1]]:
            treebankLang = treebank.split('_')[1].split('-')[0]
            if treebankLang == lang:
                counter+=1
        if counter > 1:
            has.append(item)
        else:
            hasNot.append(item)
    return [has, hasNot]

def selfScore(data):
    low = ['low']
    medium = ['medium']
    high = ['high']
    for item in data:
        if item[2][0] < 50:
            low.append(item)
        elif item[2][0] < 80:
            medium.append(item)
        else:
            high.append(item)
    return low, medium, high

def clusterSize(data):
    small = ['cluster=2']
    large = ['cluster>2']
    for item in data:
        if len(clusters[item[1]]) > 2:
            large.append(item)
        else:
            small.append(item)
    return [small, large]

def avg(data):
    return [["Avg."] + data]

print('filter & self & concat & decoder & encoder & both & ALLconc. & ALLdec. & ALLenc. & ALLboth \\\\')
data = []
for cluster in sorted(clusters):
    for treebankIdx, treebank in enumerate(sorted(clusters[cluster])):
        train, _, _, _ = myutils.getTrainTuneDevTest('data/newsplits-v2.8/' + treebank)
        if train == '':
            continue

        scores = getScores(treebank, cluster)
        data.append((treebank, cluster, scores))

def printRow(data):
    totals = [0.0] * 9
    for treebank in data[1:]:
        for i in range(len(treebank[2])):
            totals[i] += treebank[2][i]
    for i in range(len(totals)):
        totals[i] = totals[i]/len(data[1:])

    stringList = [data[0] + '-' + str(len(data)-1)]
    for score in totals:
        if score == max(totals):
            stringList.append('\\textbf{' + '{:.2f}'.format(score) + '}')
        else:
            stringList.append('{:.2f}'.format(score))
    print(' & '.join(stringList) + ' \\\\')

#for splitFunction in [trainSize, sameLang, clusterSize, selfScore]:
for splitFunction in [avg, sameLang, clusterSize, selfScore, trainSize]:
    print('\\midrule')
    for group in splitFunction(data):
        printRow(group)

#print('\\midrule')
#print(' & ' +toString('avg.', [x/counter for x in totals]))
        

