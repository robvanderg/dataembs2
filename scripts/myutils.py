import json
import os
import ast

#seeds = ['1']
#seeds = ['2', '3']
seeds = ['1', '2', '3', '4', '5']

def path2score(path):
    scores = []
    for seed in seeds:
        seedPath = path + '.' + seed +  '.eval'
        if os.path.isfile(seedPath):
            try:
                scores.append(ast.literal_eval('\n'.join(open(seedPath).readlines()))['.run/dependency/las'] * 100)
            except:
                print("Error in: " + seedPath)
        #else:
        #    print("Not found: " + seedPath)
    if len(scores) > 0:
        return sum(scores)/len(scores)
    else:
        return 0.0

def getClusters():
    data = json.load(open('scripts/ud2.2_shared_task_models-v2.json'))

    clusters = {}
    for treebank in data:
        if data[treebank][0] != data[treebank][2]:
            cluster = data[treebank][2]
            if cluster not in clusters:
                clusters[cluster] = []
            clusters[cluster].append(treebank)

    singleClusters = []
    for cluster in clusters:
        if len(clusters[cluster]) < 2:
            singleClusters.append(cluster)
    for cluster in singleClusters:
        del clusters[cluster]
    return clusters


def getTrainTuneDevTest(path):
    train = ''
    tune = ''
    dev = ''
    test = ''
    for conllFile in os.listdir(path):
        if conllFile.endswith('train.conllu'):
            train = path + '/' + conllFile
        if conllFile.endswith('tune.conllu'):
            tune = path + '/' + conllFile
        if conllFile.endswith('dev.conllu'):
            dev = path + '/' + conllFile
        if conllFile.endswith('test.conllu'):
            test = path + '/' + conllFile
    return train, tune, dev, test


def hasColumn(path, idx, threshold=.1):
    total = 0
    noWord = 0
    for line in open(path).readlines()[:5000]:
        if line[0] == '#' or len(line) < 2:
            continue
        tok = line.strip().split('\t')
        if tok[idx] == '_':
            noWord += 1
        total += 1
    return noWord/total < threshold


def getModel(path):
    if os.path.isdir(path):
        for modelDir in reversed(os.listdir(path)):
            modelPath = path + '/' + modelDir + '/model.tar.gz'
            if os.path.isfile(modelPath):
                return modelPath
    return ''


