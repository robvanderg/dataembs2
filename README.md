# Parsing with Pretrained Language Models, Multiple Datasets, and Dataset Embeddings

This is the repository with the code for the following paper: https://arxiv.org/abs/2112.03625

##  Implementation

In this repo, we have a copy of the v0.2 of [MaChAmp](https://machamp-nlp.github.io/) in which we implemented our adaptations for dataset embeddings. However, in later versions of MaChAmp (>=0.3), our implementations are included, and can more easily be used. We refer to the [https://github.com/machamp-nlp/machamp/blob/master/docs/dataset_embeds.md](README of MaChAmp)



## Reproducability
All scripts to reproduce our results can be found in `scripts/runAll.sh`. We use the approach described in detail [here](https://robvanderg.github.io/blog/repro.htm) for reproducability.

